import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
import gdb

#gdb.execute('skip -gfi /usr/include/stdio.h')
gdb.execute('set confirm off')
gdb.execute('set pagination off')
gdb.execute('file test')
#gdb.execute('skip file printf.c')
#gdb.execute('skip file printf.h')
gdb.execute('start')
#o = gdb.execute('disassemble main',to_string=True)
#print (o)
action = 's'
for i in range(0,100):
	#o = " ".join(gdb.execute('info registers rip',to_string=True).split()).split(' ')
	#print (o)
	o = gdb.execute('frame',to_string=True).split('\n')
	print('|',o,'|')
	bt = gdb.execute('bt',to_string=True).split('#')

	for t in bt:
		print(t[0:2])

	if "printf" in o[1]:
		print("GOT PRINTF")
		action = 'n'
	if 'exit.c' in o[1]:
		action = 'exit'

	if action == 's':
		gdb.execute('step',to_string=True)
	elif action == 'n':
		gdb.execute('next',to_string=True)
		action = 's'
	elif action == 'exit':
		gdb.execute('quit')
		exit()
gdb.execute('quit')

theList = [
[0,0x5555555547d8],
[0,0x5555555547e7],
 [1,0x5555555547aa],
 [1,0x5555555547b2],
[0,0x5555555547ea],
[0,0x5555555547d2],
[0,0x5555555547d8],
[0,0x5555555547e7],
 [1,0x5555555547aa],
 [1,0x5555555547b2],
[0,0x5555555547ea],
[0,0x5555555547f4],
[0,0x555555554808],
 [1,0x555555554841],
 [1,0x555555554847],
 [1,0x55555555484d],
 [1,0x555555554853],
 [1,0x555555554865],
 [1,0x55555555486d],
[0,0x555555554810],
[0,0x555555554823],
 [1,0x55555555488e],
 [1,0x5555555548b0],
  [2,0x7ffff7a48e80],
  [2,0x7ffff7a48eeb],
  [2,0x7ffff7a48ef3],
  [2,0x7ffff7a48ef9],
  [2,0x7ffff7a48f17],
  [2,0x7ffff7a48f26],

   [3,0x7ffff7a3f3ad],
   [3,0x7ffff7a3f3bc],
   [3,0x7ffff7a3f3cc],
   [3,0x7ffff7a3f3e4],
   [3,0x7ffff7a3f3fa],
   [3,0x7ffff7a3f40c],
   [3,0x7ffff7a3f411],
    [4,0x7ffff7a3f411],
   [3,0x7ffff7a3f419],
   [3,0x7ffff7a3f42d],
    [4,0x7ffff7a3f42d],
     [5,0x7ffff7b721d0],
      [6,0x7ffff7b721d2],
      [6,0x7ffff7b721d6],
      [6,0x7ffff7b721db]
];



verticies = (
    (1, -1, -1),
    (1, 1, -1),
    (-1, 1, -1),
    (-1, -1, -1),
    (1, -1, 1),
    (1, 1, 1),
    (-1, -1, 1),
    (-1, 1, 1)
    )

edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
    )

class Trace:
        def __init__(self):
                self.sections = []
                self.log = []
                self.currentSection = Section()

        def add(self,address):
                if self.currentSection.addPoint(address) == False:
                        self.sections.append(self.currentSection)
                        self.currentSection = Section()
                        self.currentSection.addPoint(address)

        def draw(self,offset,scale):
                glBegin(GL_LINES)
                glVertex3fv((-1*scale,-1*scale,offset))
                glVertex3fv((1*scale,-1*scale,offset))
                
                glVertex3fv((1*scale,-1*scale,offset))
                glVertex3fv((1*scale,1*scale,offset))
                
                glVertex3fv((1*scale,1*scale,offset))
                glVertex3fv((-1*scale,1*scale,offset))

                glVertex3fv((-1*scale,1*scale,offset))
                glVertex3fv((-1*scale,-1*scale,offset))
                glEnd()

        def render(self):
                sectionIndex = 0
                length = len(self.currentSection.address)
                self.draw(sectionIndex,length*0.01)
                for section in self.sections:
                        sectionIndex +=1
                        length = len(section.address)
                        print ("len: " + str(length) + " " + str(sectionIndex))
                        self.draw(sectionIndex*0.02,length*0.01)

class Section:
        def __init__(self):
                self.address = []

        def haveAddress(self, address):
                for adr in self.address:
                        print (str(address) + " vs " + str(adr)) 
                        if address == adr:
                                return True
                return False


        def addPoint(self, address):
                print ("add point")
                if self.haveAddress(address):
                        return False
                else:
                        self.address.append(address)
                
def fila():
	glBegin(GL_LINES)
	itter = 0
	first = theList[0][1]
	lastI = 0
	for edge in theList:
		if edge[0] != lastI:
			lastI = edge[0]
			first = edge[1]
		print(itter/200.0,edge[0]/200.0,(edge[1]-first)/200.0)
		glVertex3fv((itter/100.0,edge[0]/100.0,(edge[1]-first)/100.0))
		itter +=1
	glEnd()

def Cube():
    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glVertex3fv(verticies[vertex])
    glEnd()


def main():
    pygame.init()
    display = (800,600)
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    trace = Trace()
    for addr in theList:
            trace.add(addr[1])
    
    gluPerspective(1, (display[0]/display[1]), 0.1, 1000.0)
    glTranslatef(-0.2,0.0, -100)
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        #glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        #fila()
        #Cube()
        trace.render()
        glRotatef(1, 3, 1, 1)
        pygame.display.flip()
        pygame.time.wait(10)


main()

